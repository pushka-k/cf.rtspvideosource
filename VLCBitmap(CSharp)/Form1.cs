﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using Bwl.Framework;
using System.Threading;
using System.IO;
using System.Globalization;

namespace Cf.RTSPVideoRecord
{
    public partial class Form1 : Bwl.Framework.FormAppBase
    {
        private readonly IntegerSetting _recordTimer;
        private readonly IntegerSetting _storeTime;
        private readonly List<SourceInfo> _camerasSetup = new List<SourceInfo>();

        private readonly System.Threading.Timer _timer;
        public Form1()
        {
            InitializeComponent();
            _timer = new System.Threading.Timer(OnTimerTick);

            // Настройки камер
            var camsSettings = _storage.CreateChildStorage("camsSettings", "Настройки камер");
            var camCount = camsSettings.CreateIntegerSetting("CamCount", 1, "Количество камер");

            for (int i = 0; i < camCount.Value; i++)
            {
                var cam = new SourceInfo(camsSettings, i);
                _camerasSetup.Add(cam);
                cam.FrameReceived += FrameReceivedHandler;
                cam.RecStatusChanged += ShowCamStatus;
            }
            _btnPauseRecord.Enabled = false;

            // Настройки записей
            var recordsSettings = _storage.CreateChildStorage("recordsSettings", "Настройки записи");
            _storeTime = recordsSettings.CreateIntegerSetting("StoreTime", 30, "Сколько дней хранить локальное видео");
            _recordTimer = recordsSettings.CreateIntegerSetting("RecordTimer", 60, "Максимальное время записи", "В минутах");


        }

        private void FrameReceivedHandler(int camId, Bitmap frame)
        {
            if (_showImgFromCamCheckBox.Checked)
            {
                try
                {
                    _camImagePictureBox.Image = camId == _camIdUpDown.Value - 1 ? frame : null;
                }
                catch { }
            }
        }

        private void ShowCamStatus(int camId, bool status)
        {
            if (_showImgFromCamCheckBox.Checked)
            {
                if (camId == _camIdUpDown.Value - 1)
                {
                    _recordStatusLabel.Text = status ? "Статус: Работает" : "Статус: Остановлено";
                }
            }
        }

        private void StartRecord(object sender = null, EventArgs e = null)
        {
            try
            {
                foreach (var cam in _camerasSetup)
                {
                    try
                    {
                        cam.Start();
                    }
                    catch (Exception ex)
                    {
                        _logger.AddError(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.AddError(ex.ToString());
            }
        }

        private void RestartRecord(object sender = null, EventArgs e = null)
        {
            try
            {
                foreach (var cam in _camerasSetup)
                {
                    try
                    {
                        cam.Stop();
                        cam.Start();
                    }
                    catch (Exception ex)
                    {
                        _logger.AddError(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.AddError(ex.ToString());
            }
        }

        private void PauseRecord(object sender = null, EventArgs e = null)
        {
            try
            {
                foreach (var cam in _camerasSetup)
                {
                    try
                    {
                        cam.Pause();
                    }
                    catch (Exception ex)
                    {
                        _logger.AddError(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.AddError(ex.ToString());
            }
        }

        private void StopRecord(object sender = null, EventArgs e = null)
        {
            try
            {
                foreach (var cam in _camerasSetup)
                {
                    try
                    {
                        cam.Stop();
                    }
                    catch (Exception ex)
                    {
                        _logger.AddError(ex.ToString());
                    }
                }
                _camImagePictureBox.Image = null;
            }
            catch (Exception ex)
            {
                _logger.AddError(ex.ToString());
            }
        }

        private void OnTimerTick(object state = null)
        {
            RestartRecord();

            DeleteOldRecords();

            try
            {
                _timer.Change(new TimeSpan(0, _recordTimer.Value, 0), new TimeSpan(-1));
            }
            catch (Exception ex)
            {
                _logger.AddError(ex.ToString());
            }
        }

        private void DeleteOldRecords()
        {
            try
            {
                var n = DateTime.Now;
                var files = Directory.GetFiles(SourceInfo.RecordDir, "*.avi");
                foreach (var f in files)
                {
                    try
                    {
                        var fTime = DateTime.ParseExact(f.Substring(f.Length - 23, 19), SourceInfo.DtFormat, CultureInfo.InvariantCulture);
                        if ((n - fTime).TotalDays > _storeTime.Value)
                        {
                            File.Delete(f);
                        }
                    }
                    catch (Exception e2)
                    {
                        _logger.AddError(e2.ToString());
                    }
                }
            }
            catch (Exception e1)
            {
                _logger.AddError(e1.ToString());
            }
        }

        private void BtnPauseRecordClick(object sender, EventArgs e)
        {
            OnTimerTick();
        }

        private void ShowImgFromCamCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            if (!_showImgFromCamCheckBox.Checked)
            {
                _camImagePictureBox.Image = null;
            }

        }

        private void Form1FormClosing(object sender, FormClosingEventArgs e)
        {
            StopRecord();
        }
    }
}
