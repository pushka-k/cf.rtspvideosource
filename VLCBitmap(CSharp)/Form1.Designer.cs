﻿namespace Cf.RTSPVideoRecord
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnPauseRecord = new System.Windows.Forms.Button();
            this._btnStopRecord = new System.Windows.Forms.Button();
            this._btnStartRecord = new System.Windows.Forms.Button();
            this._showImgFromCamCheckBox = new System.Windows.Forms.CheckBox();
            this._camImagePictureBox = new System.Windows.Forms.PictureBox();
            this._camIdUpDown = new System.Windows.Forms.NumericUpDown();
            this._recordStatusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._camImagePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._camIdUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // logWriter
            // 
            this.logWriter.Location = new System.Drawing.Point(9, 466);
            this.logWriter.Size = new System.Drawing.Size(704, 208);
            // 
            // _btnPauseRecord
            // 
            this._btnPauseRecord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._btnPauseRecord.Location = new System.Drawing.Point(9, 395);
            this._btnPauseRecord.Name = "_btnPauseRecord";
            this._btnPauseRecord.Size = new System.Drawing.Size(704, 25);
            this._btnPauseRecord.TabIndex = 1;
            this._btnPauseRecord.Text = "Приостановить запись видео ";
            this._btnPauseRecord.UseVisualStyleBackColor = true;
            this._btnPauseRecord.Click += new System.EventHandler(this.PauseRecord);
            // 
            // _btnStopRecord
            // 
            this._btnStopRecord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._btnStopRecord.Location = new System.Drawing.Point(9, 426);
            this._btnStopRecord.Name = "_btnStopRecord";
            this._btnStopRecord.Size = new System.Drawing.Size(704, 25);
            this._btnStopRecord.TabIndex = 2;
            this._btnStopRecord.Text = "Остановить запись видео ";
            this._btnStopRecord.UseVisualStyleBackColor = true;
            this._btnStopRecord.Click += new System.EventHandler(this.StopRecord);
            // 
            // _btnStartRecord
            // 
            this._btnStartRecord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._btnStartRecord.Location = new System.Drawing.Point(9, 364);
            this._btnStartRecord.Name = "_btnStartRecord";
            this._btnStartRecord.Size = new System.Drawing.Size(704, 25);
            this._btnStartRecord.TabIndex = 3;
            this._btnStartRecord.Text = "Запустить запись видео ";
            this._btnStartRecord.UseVisualStyleBackColor = true;
            this._btnStartRecord.Click += new System.EventHandler(this.StartRecord);
            // 
            // _showImgFromCamCheckBox
            // 
            this._showImgFromCamCheckBox.AutoSize = true;
            this._showImgFromCamCheckBox.Checked = true;
            this._showImgFromCamCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._showImgFromCamCheckBox.Location = new System.Drawing.Point(12, 27);
            this._showImgFromCamCheckBox.Name = "_showImgFromCamCheckBox";
            this._showImgFromCamCheckBox.Size = new System.Drawing.Size(212, 17);
            this._showImgFromCamCheckBox.TabIndex = 4;
            this._showImgFromCamCheckBox.Text = "Показывать изображение с камеры";
            this._showImgFromCamCheckBox.UseVisualStyleBackColor = true;
            this._showImgFromCamCheckBox.CheckedChanged += new System.EventHandler(this.ShowImgFromCamCheckBoxCheckedChanged);
            // 
            // _camImagePictureBox
            // 
            this._camImagePictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._camImagePictureBox.BackColor = System.Drawing.Color.Black;
            this._camImagePictureBox.Location = new System.Drawing.Point(9, 50);
            this._camImagePictureBox.Name = "_camImagePictureBox";
            this._camImagePictureBox.Size = new System.Drawing.Size(704, 308);
            this._camImagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._camImagePictureBox.TabIndex = 5;
            this._camImagePictureBox.TabStop = false;
            // 
            // _camIdUpDown
            // 
            this._camIdUpDown.Location = new System.Drawing.Point(230, 26);
            this._camIdUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._camIdUpDown.Name = "_camIdUpDown";
            this._camIdUpDown.Size = new System.Drawing.Size(120, 20);
            this._camIdUpDown.TabIndex = 6;
            this._camIdUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // _recordStatusLabel
            // 
            this._recordStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._recordStatusLabel.AutoSize = true;
            this._recordStatusLabel.Location = new System.Drawing.Point(599, 28);
            this._recordStatusLabel.Name = "_recordStatusLabel";
            this._recordStatusLabel.Size = new System.Drawing.Size(114, 13);
            this._recordStatusLabel.TabIndex = 8;
            this._recordStatusLabel.Text = "Статус: Остановлено";
            this._recordStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 683);
            this.Controls.Add(this._recordStatusLabel);
            this.Controls.Add(this._camIdUpDown);
            this.Controls.Add(this._camImagePictureBox);
            this.Controls.Add(this._showImgFromCamCheckBox);
            this.Controls.Add(this._btnStartRecord);
            this.Controls.Add(this._btnStopRecord);
            this.Controls.Add(this._btnPauseRecord);
            this.Name = "Form1";
            this.Text = "RTSP видео запись CleverFlow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1FormClosing);
            this.Controls.SetChildIndex(this._btnPauseRecord, 0);
            this.Controls.SetChildIndex(this.logWriter, 0);
            this.Controls.SetChildIndex(this._btnStopRecord, 0);
            this.Controls.SetChildIndex(this._btnStartRecord, 0);
            this.Controls.SetChildIndex(this._showImgFromCamCheckBox, 0);
            this.Controls.SetChildIndex(this._camImagePictureBox, 0);
            this.Controls.SetChildIndex(this._camIdUpDown, 0);
            this.Controls.SetChildIndex(this._recordStatusLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this._camImagePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._camIdUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _btnPauseRecord;
        private System.Windows.Forms.Button _btnStopRecord;
        private System.Windows.Forms.Button _btnStartRecord;
        private System.Windows.Forms.CheckBox _showImgFromCamCheckBox;
        private System.Windows.Forms.PictureBox _camImagePictureBox;
        private System.Windows.Forms.NumericUpDown _camIdUpDown;
        private System.Windows.Forms.Label _recordStatusLabel;
    }
}

