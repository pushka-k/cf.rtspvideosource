﻿using Bwl.Framework;
using Declarations;
using Declarations.Enums;
using Declarations.Media;
using Declarations.Players;
using Implementation;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

using Cf.RTSPVideoRecord.Properties;

using Microsoft.VisualBasic;

namespace Cf.RTSPVideoRecord
{
    class SourceInfo
    {
        private static readonly IMediaPlayerFactory Factory;
        private static IMemoryInputMedia _inputMedia;
        private static StringSetting _recordDir;
        private readonly StringSetting _rtspString;
        private IVideoPlayer _sourcePlayer;
        private readonly int _camId;
        private int _width;
        private int _height;
        private readonly BooleanSetting _copyToRemoteFolder;
        private readonly BooleanSetting _isNeedConnection;
        private readonly StringSetting _remoteFolderPath;
        private readonly StringSetting _remoteServerName;
        private readonly StringSetting _login;
        private readonly PasswordSetting _password;

        public delegate void FrameReceivedHandler(int camId, Bitmap frame);
        public event FrameReceivedHandler FrameReceived;

        public delegate void RecordStatusChanged(int camId, bool isWorking);
        public event RecordStatusChanged RecStatusChanged;

        public static readonly string DtFormat = "HH-mm-ss dd.MM.yyyy";

        public static string RecordDir => _recordDir.Value;

        private void SetupInput(BitmapFormat format)
        {
            var streamInfo = new StreamInfo
            {
                Category = StreamCategory.Video,
                Codec = VideoCodecs.BGR24,
                Width = format.Width,
                Height = format.Height,
                Size = format.ImageSize
            };

            _inputMedia.Initialize(streamInfo);
            _inputMedia.SetExceptionHandler(OnErrorCallback);
        }

        private void OpenSourceMedia(string url, string[] options)
        {
            IMedia media = Factory.CreateMedia<IMedia>(url, options);
            _inputMedia = Factory.CreateMedia<IMemoryInputMedia>(MediaStrings.IMEM);
            _sourcePlayer = Factory.CreatePlayer<IVideoPlayer>();
            _sourcePlayer.Mute = true;
            SetupOutput(_sourcePlayer.CustomRendererEx);
            _sourcePlayer.Open(media);
            _sourcePlayer.Play();
        }

        private BitmapFormat OnSetupCallback(BitmapFormat format)
        {
            SetupInput(format);
            _width = format.Width;
            _height = format.Height;
            return new BitmapFormat(format.Width, format.Height, ChromaType.RV24);
        }

        private void OnErrorCallback(Exception error)
        {
            MessageBox.Show(error.Message);
        }

        private void OnNewFrameCallback(PlanarFrame frame)
        {
            if (frame.Planes != null)
            {
                int formatPixel_RGB = 3; // зависит от формата(PixelFormat.Format24bppRgb - всего 3 цвета)
                int stride = _width * formatPixel_RGB;
                Bitmap bmp = new Bitmap(_width, _height, stride, PixelFormat.Format24bppRgb, frame.Planes[0]);
                FrameReceived?.Invoke(_camId, bmp);
            }
        }

        static SourceInfo()
        {
            Factory = new MediaPlayerFactory(true);
        }

        public SourceInfo(SettingsStorage storage, int id)
        {
            var camSettings = storage.CreateChildStorage("camSettings" + id, string.Concat("Настройки камеры ", id + 1));
            _camId = id;
            if (_recordDir == null)
            {
                _recordDir = camSettings.CreateStringSetting("RecordDir", Path.GetFullPath(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ".."), "video", "cam" + (id + 1))), "Путь для хранения видео с камеры");
            }
            _rtspString = camSettings.CreateStringSetting("RtspStr_" + id, "rtsp://20.20.25.200/", "Адрес подключения (RTSP)");
            _copyToRemoteFolder = camSettings.CreateBooleanSetting("copyToRemoteFolder", false, "Копирование в удалённую папку");
            _isNeedConnection = camSettings.CreateBooleanSetting("isNeedConnection", false, "Необходимо подключение");
            _remoteFolderPath = camSettings.CreateStringSetting("remoteFolderPath", @"\\CFSERVER\Shared\Work Data Big\VideoFromStorage", "Удалённая папка");
            _remoteServerName = camSettings.CreateStringSetting("remoteServerName", @"\\CFSERVER", "Имя удалённого сервера");
            _login = camSettings.CreateStringSetting("login", "user", "Имя пользователя для удалённого сервера");
            _password = camSettings.CreatePasswordSetting("password", "", "Пароль для удалённого сервера");
        }

        private void SetupOutput(IMemoryRendererEx iMemoryRenderer)
        {
            iMemoryRenderer.SetFormatSetupCallback(OnSetupCallback);
            iMemoryRenderer.SetExceptionHandler(OnErrorCallback);
            iMemoryRenderer.SetCallback(OnNewFrameCallback);
        }

        public void Start()
        {

            if (_sourcePlayer != null)
            {
                _sourcePlayer.Play();
            }
            else
            {
                if (!Directory.Exists(_recordDir.Value))
                {
                    Directory.CreateDirectory(_recordDir.Value);
                }

                var filepath = Path.Combine(_recordDir.Value, "Cam_" + _camId + "_Temp", ".avi");
                if (File.Exists(filepath)) File.Delete(filepath);

                string opt = ":sout=#duplicate{dst=std{access=file,mux=ts,dst=\"" + Path.Combine(_recordDir.Value, "Cam_" + _camId + "_Temp") + ".avi\"},dst=display}";
                //string opt = ":sout=#duplicate{dst=std{access=file,mux=ts,dst=\"" + Path.Combine(_recordDir.Value, "Cam_" + _camId + "_Temp") + ".avi\"},dst=nodisplay}";
                //string opt = ":sout=#duplicate{dst=std{access=file,mux=ts},dst=display}";

                string[] options = new string[] { opt };
                OpenSourceMedia(_rtspString.Value, options);
            }
            RecStatusChanged?.Invoke(_camId, true);
        }

        public void Stop()
        {
            if (_sourcePlayer != null)
            {
                _sourcePlayer.Stop();
                StringBuilder desc = new StringBuilder();
                string about = @"\/*&%";

                while (isStringContainsInvalidChars(about))
                {
                    about = Interaction.InputBox("Введите общее описание (кратко)", "Общее описание");
                    if (isStringContainsInvalidChars(about)) MessageBox.Show(Resources.fileNameWarning);
                }

                var date = Interaction.InputBox("Введите дату счёта", "Дата счёта");
                var num = Interaction.InputBox("Введите номер счёта", "Номер счёта");
                var text = string.Concat("Счёт №", num, " от ", date);
                if (isStringContainsInvalidChars(text)) MessageBox.Show(Resources.fileNameWarning);

                desc.AppendLine("Общее описание: " + about);
                desc.AppendLine("Дата и номер счёта: " + text);
                desc.AppendLine("Компания-поставщик: " + Interaction.InputBox("Укажите компанию-поставщика", "Компания-поставщик"));
                _sourcePlayer.Dispose();
                _sourcePlayer = null;
                string pathToFile = Path.Combine(_recordDir.Value, "Cam_" + _camId + "_Temp.avi");
                string newPathToFile = Path.Combine(_recordDir.Value, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " (" + about + ")");
                if (!Directory.Exists(newPathToFile))
                    Directory.CreateDirectory(newPathToFile);
                File.Move(pathToFile, Path.Combine(newPathToFile, text + ".avi"));

                StreamWriter file = new StreamWriter(Path.Combine(newPathToFile, "description.txt"));
                file.WriteLine(desc.ToString());
                file.Close();
                file.Dispose();

                if (_copyToRemoteFolder.Value) CopyDirectoryToRemoteServer(newPathToFile, _remoteFolderPath.Value);
                RecStatusChanged?.Invoke(_camId, false);
            }
        }

        public void Pause()
        {

            if (_sourcePlayer != null)
            {
                _sourcePlayer.Pause();
                RecStatusChanged?.Invoke(_camId, false);
            }
        }

        private void CopyDirectoryToRemoteServer(string originalFolderPath, string copyToPath)
        {
            var connectionNeeded = _isNeedConnection.Value;

            NetworkConnection connection = null;

            if (connectionNeeded) connection = new NetworkConnection(_remoteServerName.Value, new NetworkCredential(_login.Value, _password.Pass));

            string folderName = Path.GetFileName(originalFolderPath);
            if (folderName != null)
            {
                string remoteFolder = Path.Combine(copyToPath, folderName);
                Directory.CreateDirectory(remoteFolder);
                foreach (string dirPath in Directory.GetDirectories(originalFolderPath, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(dirPath.Replace(originalFolderPath, remoteFolder));
                }
                foreach (string newPath in Directory.GetFiles(originalFolderPath, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(originalFolderPath, remoteFolder), true);
                }
            }

            connection?.Dispose();
        }

        private bool isStringContainsInvalidChars(string str)
        {
            var pathChars = Path.GetInvalidPathChars();
            var filenameChars = Path.GetInvalidFileNameChars();

            var result = pathChars.Any(str.Contains);
            if (!result) result = filenameChars.Any(str.Contains);

            return result;
        }
    }


}
